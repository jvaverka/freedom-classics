+++
author = "Jacob Vaverka"
mintoclevel = 2
prepath = "freedom-classics"

ignore = ["node_modules/"]

# RSS (the website_{title, descr, url} must be defined to get RSS)
generate_rss = false
website_title = "Freedom Classics"
website_descr = "Freedom Classic Cars website"
website_url   = "https://jvaverka.gitlab.io/freedom-classics"
+++

\newcommand{\card}[3]{
  @@card
    ![#1](/assets/vehicles/!#2.jpg)
    @@container
      ~~~
      <h2>#1</h2>
      ~~~
      @@title #1 @@
      @@price #3 @@
      ~~~
      <ul>
      ~~~
      for attribute in #4
      ~~~
      <li>attribute</li>
      ~~~
      end
      ~~~
      </ul>
      ~~~
    @@
  @@
}

\newcommand{\car}[2]{
    @@car
       ![#1](/inventory/!#2.jpg)
    @@
}

+++
title = "Freedom Classics"
tags = ["about"]
+++

## Welcome

Freedom Classics LLC is a classic car dealer based out of Flowery Branch, Georgia.
Please checkout our [inventory](/inventory/) and [reach out](/about/) if you are
interested to learn more!

~~~ <div class="container"> ~~~

![BelAir](/assets/cover/cover.jpg)
![Benz](/assets/cover/benz.jpg)
![Line](/assets/cover/line.jpg)
![Rover](/assets/cover/rover.jpg)
![Red](/assets/cover/red.jpg)
![RedGreen](/assets/cover/red-green.jpg)
![Ford](/assets/cover/ford.jpg)
![Chevy](/assets/cover/chevy.jpg)

~~~ </div> ~~~

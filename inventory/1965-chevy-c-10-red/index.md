+++
title = "Inventory | 1965 Chevy C-10"
vehicle_name = "1965 Chevy C-10 (Red)"
vehicle_photo = "1965_Chevy_C10_9I0A7437"
vehicle_price = "48,500"
vehicle_attr = [
    "LS swap",
    "Leather seats",
    "Custom gauges",
    "Automatic transmission",
    "American racing wheels",
    "Wood bed",
    "Vintage air",
]
+++

{{ vehicle_card 1965-chevy-c-10-red }}

## Gallery

~~~ <div class="container"> ~~~

<!-- ![1965_Chevy_C10_9I0A7434](/assets/vehicles/1965-chevy-c-10-red/1965_Chevy_C10_9I0A7434.jpg) -->
![1965_Chevy_C10_9I0A7435](/assets/vehicles/1965-chevy-c-10-red/1965_Chevy_C10_9I0A7435.jpg)
![1965_Chevy_C10_9I0A7436](/assets/vehicles/1965-chevy-c-10-red/1965_Chevy_C10_9I0A7436.jpg)
<!-- ![1965_Chevy_C10_9I0A7437](/assets/vehicles/1965-chevy-c-10-red/1965_Chevy_C10_9I0A7437.jpg) -->
<!-- ![1965_Chevy_C10_9I0A7438](/assets/vehicles/1965-chevy-c-10-red/1965_Chevy_C10_9I0A7438.jpg) -->
<!-- ![1965_Chevy_C10_9I0A7439](/assets/vehicles/1965-chevy-c-10-red/1965_Chevy_C10_9I0A7439.jpg) -->
![1965_Chevy_C10_9I0A7440](/assets/vehicles/1965-chevy-c-10-red/1965_Chevy_C10_9I0A7440.jpg)
![1965_Chevy_C10_9I0A7441](/assets/vehicles/1965-chevy-c-10-red/1965_Chevy_C10_9I0A7441.jpg)
<!-- ![1965_Chevy_C10_9I0A7442](/assets/vehicles/1965-chevy-c-10-red/1965_Chevy_C10_9I0A7442.jpg) -->
<!-- ![1965_Chevy_C10_9I0A7443](/assets/vehicles/1965-chevy-c-10-red/1965_Chevy_C10_9I0A7443.jpg) -->
![1965_Chevy_C10_9I0A7444](/assets/vehicles/1965-chevy-c-10-red/1965_Chevy_C10_9I0A7444.jpg)

~~~ </div> ~~~


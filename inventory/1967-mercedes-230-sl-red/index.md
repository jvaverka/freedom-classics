+++
title = "Inventory | 1967 Mercedes 230 SL"
vehicle_name = "1967 Mercedes 230 SL (Red)"
vehicle_photo = "1967_Mercedes_230_SL_9I0A7411"
vehicle_price = "102,000"
vehicle_attr = [
    "Hard top and soft top",
    "Professionally restored to original state",
    "New Leather interior",
    "New carpet",
    "76,000 original miles",
    "3rd owner",
    "All maintenance records",
    "All manuals",
    "Automatic transmission",
    "Air conditioning",
]
+++

{{ vehicle_card 1967-mercedes-230-sl-red }}

## Gallery

~~~ <div class="container"> ~~~

![1967_Mercedes_230_SL_9I0A7405](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7405.jpg)
<!-- ![1967_Mercedes_230_SL_9I0A7406](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7406.jpg) -->
<!-- ![1967_Mercedes_230_SL_9I0A7407](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7407.jpg) -->
![1967_Mercedes_230_SL_9I0A7408](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7408.jpg)
<!-- ![1967_Mercedes_230_SL_9I0A7409](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7409.jpg) -->
![1967_Mercedes_230_SL_9I0A7410](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7410.jpg)
<!-- ![1967_Mercedes_230_SL_9I0A7411](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7411.jpg) -->
![1967_Mercedes_230_SL_9I0A7412](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7412.jpg)
![1967_Mercedes_230_SL_9I0A7413](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7413.jpg)
![1967_Mercedes_230_SL_9I0A7414](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7414.jpg)
![1967_Mercedes_230_SL_9I0A7415](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7415.jpg)
<!-- ![1967_Mercedes_230_SL_9I0A7416](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7416.jpg) -->
<!-- ![1967_Mercedes_230_SL_9I0A7417](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7417.jpg) -->
![1967_Mercedes_230_SL_9I0A7419](/assets/vehicles/1967-mercedes-230-sl-red/1967_Mercedes_230_SL_9I0A7419.jpg)

~~~ </div> ~~~


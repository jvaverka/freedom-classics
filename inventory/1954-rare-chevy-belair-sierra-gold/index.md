+++
title = "Inventory | 1954 RARE Chevy BelAir Convertible (Sierra Gold)"
vehicle_name = "1954 RARE Chevy BelAir Convertible (Sierra Gold)"
vehicle_photo = "1954_RARE_Chevy_BelAir_9I0A7553"
vehicle_price = "Call for quote"
vehicle_attr = [
    "Frame off restoration with documentation",
    "Factor air (RARE)",
    "Sierra gold and cream paint",
    "Perfect restoration",
    "Too much to list",
]
+++

{{ vehicle_card 1954-rare-chevy-belair-sierra-gold }}

## Gallery

~~~ <div class="container"> ~~~

<!-- ![1954_RARE_Chevy_BelAir_9I0A7551](/assets/vehicles/1954-rare-chevy-belair-sierra-gold/1954_RARE_Chevy_BelAir_9I0A7551.jpg) -->
![1954_RARE_Chevy_BelAir_9I0A7552](/assets/vehicles/1954-rare-chevy-belair-sierra-gold/1954_RARE_Chevy_BelAir_9I0A7552.jpg)
![1954_RARE_Chevy_BelAir_9I0A7553](/assets/vehicles/1954-rare-chevy-belair-sierra-gold/1954_RARE_Chevy_BelAir_9I0A7553.jpg)
<!-- ![1954_RARE_Chevy_BelAir_9I0A7554](/assets/vehicles/1954-rare-chevy-belair-sierra-gold/1954_RARE_Chevy_BelAir_9I0A7554.jpg) -->
<!-- ![1954_RARE_Chevy_BelAir_9I0A7555](/assets/vehicles/1954-rare-chevy-belair-sierra-gold/1954_RARE_Chevy_BelAir_9I0A7555.jpg) -->
![1954_RARE_Chevy_BelAir_9I0A7556](/assets/vehicles/1954-rare-chevy-belair-sierra-gold/1954_RARE_Chevy_BelAir_9I0A7556.jpg)
![1954_RARE_Chevy_BelAir_9I0A7557](/assets/vehicles/1954-rare-chevy-belair-sierra-gold/1954_RARE_Chevy_BelAir_9I0A7557.jpg)
<!-- ![1954_RARE_Chevy_BelAir_9I0A7558](/assets/vehicles/1954-rare-chevy-belair-sierra-gold/1954_RARE_Chevy_BelAir_9I0A7558.jpg) -->
![1954_RARE_Chevy_BelAir_9I0A7559](/assets/vehicles/1954-rare-chevy-belair-sierra-gold/1954_RARE_Chevy_BelAir_9I0A7559.jpg)
![1954_RARE_Chevy_BelAir_9I0A7560](/assets/vehicles/1954-rare-chevy-belair-sierra-gold/1954_RARE_Chevy_BelAir_9I0A7560.jpg)

~~~ </div> ~~~


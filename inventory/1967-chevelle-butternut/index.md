+++
title = "Inventory | 1967 Chevelle"
vehicle_name = "1967 Chevelle"
vehicle_photo = "1967_Chevelle_9I0A7549"
vehicle_price = "Call for quote"
vehicle_attr = [
    "Butternut yellow paint",
    "136 car",
    "502 engine",
    "Tremec transmission",
    "Transmission cooler",
    "Vintage air",
    "Frame off restoration",
]
+++

{{ vehicle_card 1967-chevelle-butternut }}

## Gallery

~~~ <div class="container"> ~~~

![1967_Chevelle_a](/assets/vehicles/1967-chevelle-butternut/1967_Chevelle_9I0A7548.jpg)
![1967_Chevelle_b](/assets/vehicles/1967-chevelle-butternut/1967_Chevelle_9I0A7550.jpg)

~~~ </div> ~~~


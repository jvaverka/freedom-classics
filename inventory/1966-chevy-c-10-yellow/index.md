+++
title = "Inventory | 1966 Chevy C-10"
vehicle_name = "1966 Chevy C-10 (Yellow)"
vehicle_photo = "1966_Chevy_C10_9I0A7518"  
vehicle_price = "34,000"
vehicle_attr = [
    "350 rebuilt motor",
    "Air conditioning",
    "New radial tires",
    "Rally wheels",
    "Cloth Seats",
    "Bed cover",
    "Wood Bed",
    "71,600 miles",
    "New paint",
]
+++

{{ vehicle_card 1966-chevy-c-10-yellow }}

## Gallery

~~~ <div class="container"> ~~~

![1966_Chevy_C10_a](/assets/vehicles/1966-chevy-c-10-yellow/1966_Chevy_C10_9I0A7490.jpg)
![1966_Chevy_C10_b](/assets/vehicles/1966-chevy-c-10-yellow/1966_Chevy_C10_9I0A7510.jpg)
![1966_Chevy_C10_c](/assets/vehicles/1966-chevy-c-10-yellow/1966_Chevy_C10_9I0A7511.jpg)
![1966_Chevy_C10_d](/assets/vehicles/1966-chevy-c-10-yellow/1966_Chevy_C10_9I0A7512.jpg)
![1966_Chevy_C10_e](/assets/vehicles/1966-chevy-c-10-yellow/1966_Chevy_C10_9I0A7513.jpg)
![1966_Chevy_C10_f](/assets/vehicles/1966-chevy-c-10-yellow/1966_Chevy_C10_9I0A7514.jpg)
![1966_Chevy_C10_g](/assets/vehicles/1966-chevy-c-10-yellow/1966_Chevy_C10_9I0A7516.jpg)
![1966_Chevy_C10_h](/assets/vehicles/1966-chevy-c-10-yellow/1966_Chevy_C10_9I0A7517.jpg)

~~~ </div> ~~~


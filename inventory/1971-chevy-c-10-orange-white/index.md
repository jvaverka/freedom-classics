+++
title = "Inventory | 1971 Chevy C-10"
vehicle_name = "1971 Chevy C-10 (Orange & White)"
vehicle_photo = "1971_Chevy_C10_9I0A7535"  
vehicle_price = "41,000"
vehicle_attr = [
    "350 engine",
    "Air conditioning",
    "Houndstooth seats",
    "Automatic transmission",
    "New tires",
    "Ridler wheels ",
]
+++

{{ vehicle_card 1971-chevy-c-10-orange-white }}

## Gallery

~~~ <div class="container"> ~~~

![1971_Chevy_C10_9I0A7534](/assets/vehicles/1971-chevy-c-10-orange-white/1971_Chevy_C10_9I0A7534.jpg)
<!-- ![1971_Chevy_C10_9I0A7535](/assets/vehicles/1971-chevy-c-10-orange-white/1971_Chevy_C10_9I0A7535.jpg) -->
![1971_Chevy_C10_9I0A7536](/assets/vehicles/1971-chevy-c-10-orange-white/1971_Chevy_C10_9I0A7536.jpg)
<!-- ![1971_Chevy_C10_9I0A7537](/assets/vehicles/1971-chevy-c-10-orange-white/1971_Chevy_C10_9I0A7537.jpg) -->
![1971_Chevy_C10_9I0A7538](/assets/vehicles/1971-chevy-c-10-orange-white/1971_Chevy_C10_9I0A7538.jpg)
<!-- ![1971_Chevy_C10_9I0A7539](/assets/vehicles/1971-chevy-c-10-orange-white/1971_Chevy_C10_9I0A7539.jpg) -->
![1971_Chevy_C10_9I0A7540](/assets/vehicles/1971-chevy-c-10-orange-white/1971_Chevy_C10_9I0A7540.jpg)
<!-- ![1971_Chevy_C10_9I0A7541](/assets/vehicles/1971-chevy-c-10-orange-white/1971_Chevy_C10_9I0A7541.jpg) -->

~~~ </div> ~~~


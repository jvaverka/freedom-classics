+++
title = "Inventory | 1980 Datsun 280 ZX"
vehicle_name = "1980 Datsun 280 ZX (Red)"
vehicle_photo = "1980_Datsun_280ZX_9I0A7459"
vehicle_price = "30,000"
vehicle_attr = [
    "anniversary addition",
    "air conditioning",
    "46,000 original miles",
    "5 speed transmission",
    "red interior leather",
    "parked in garage for 21 years",
]
+++

{{ vehicle_card 1980-datsun-280zx-red }}

## Gallery

~~~ <div class="container"> ~~~

<!-- ![1980_Datsun_280ZX_9I0A7457](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7457.jpg) -->
![1980_Datsun_280ZX_9I0A7458](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7458.jpg)
<!-- ![1980_Datsun_280ZX_9I0A7459](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7459.jpg) -->
<!-- ![1980_Datsun_280ZX_9I0A7460](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7460.jpg) -->
![1980_Datsun_280ZX_9I0A7461](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7461.jpg)
<!-- ![1980_Datsun_280ZX_9I0A7462](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7462.jpg) -->
![1980_Datsun_280ZX_9I0A7463](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7463.jpg)
<!-- ![1980_Datsun_280ZX_9I0A7464](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7464.jpg) -->
![1980_Datsun_280ZX_9I0A7465](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7465.jpg)
<!-- ![1980_Datsun_280ZX_9I0A7466](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7466.jpg) -->
<!-- ![1980_Datsun_280ZX_9I0A7467](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7467.jpg) -->
![1980_Datsun_280ZX_9I0A7468](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7468.jpg)
<!-- ![1980_Datsun_280ZX_9I0A7469](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7469.jpg) -->
<!-- ![1980_Datsun_280ZX_9I0A7470](/assets/vehicles/1980-datsun-280zx-red/1980_Datsun_280ZX_9I0A7470.jpg) -->

~~~ </div> ~~~


+++
title = "Inventory | 1960 Chevy Impala (Red)"
vehicle_name = "1960 Chevy Impala (Red)"
vehicle_photo = "1960_Chevy_Impala_9I0A7495"
vehicle_price = "54,000"
vehicle_attr = [
    "305 motor",
    "Automatic transmission",
    "Vintage air",
    "Houndstooth interior",
    "Been housed in museum",
]
+++

{{ vehicle_card 1960-chevy-impala-red }}

## Gallery

~~~ <div class="container"> ~~~

![1960_Chevy_Impala_9I0A7491](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7491.jpg)
<!-- ![1960_Chevy_Impala_9I0A7492](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7492.jpg) -->
![1960_Chevy_Impala_9I0A7493](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7493.jpg)
<!-- ![1960_Chevy_Impala_9I0A7494](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7494.jpg) -->
<!-- ![1960_Chevy_Impala_9I0A7495](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7495.jpg) -->
<!-- ![1960_Chevy_Impala_9I0A7496](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7496.jpg) -->
![1960_Chevy_Impala_9I0A7497](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7497.jpg)
<!-- ![1960_Chevy_Impala_9I0A7498](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7498.jpg) -->
<!-- ![1960_Chevy_Impala_9I0A7499](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7499.jpg) -->
<!-- ![1960_Chevy_Impala_9I0A7500](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7500.jpg) -->
<!-- ![1960_Chevy_Impala_9I0A7501](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7501.jpg) -->
<!-- ![1960_Chevy_Impala_9I0A7502](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7502.jpg) -->
<!-- ![1960_Chevy_Impala_9I0A7503](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7503.jpg) -->
![1960_Chevy_Impala_9I0A7504](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7504.jpg)
<!-- ![1960_Chevy_Impala_9I0A7505](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7505.jpg) -->
![1960_Chevy_Impala_9I0A7506](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7506.jpg)
<!-- ![1960_Chevy_Impala_9I0A7507](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7507.jpg) -->
<!-- ![1960_Chevy_Impala_9I0A7508](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7508.jpg) -->
<!-- ![1960_Chevy_Impala_9I0A7509](/assets/vehicles/1960-chevy-impala-red/1960_Chevy_Impala_9I0A7509.jpg) -->

~~~ </div> ~~~


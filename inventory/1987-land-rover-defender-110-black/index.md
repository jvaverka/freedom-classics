+++
title = "Inventory | 1987 Land Rover Defender 110 (Black)"
vehicle_name = "1987 Land Rover Defender 110 (Black)"
vehicle_photo = "1987_Land_Rover_Defender_110_9I0A7450"
vehicle_price = "73,500"
vehicle_attr = [
    "Frame off restoration",
    "200 TDI diesel engine",
    "Steel bumpers",
    "warn winch",
    "Black cloth seats",
    "Carpet",
    "5 speed manual transmission",
]
+++

{{ vehicle_card 1987-land-rover-defender-110-black }}

## Gallery

~~~ <div class="container"> ~~~

![1987_Land_Rover_Defender_110_9I0A7445](/assets/vehicles/1987-land-rover-defender-110-black/1987_Land_Rover_Defender_110_9I0A7445.jpg)
<!-- ![1987_Land_Rover_Defender_110_9I0A7446](/assets/vehicles/1987-land-rover-defender-110-black/1987_Land_Rover_Defender_110_9I0A7446.jpg) -->
![1987_Land_Rover_Defender_110_9I0A7448](/assets/vehicles/1987-land-rover-defender-110-black/1987_Land_Rover_Defender_110_9I0A7448.jpg)
<!-- ![1987_Land_Rover_Defender_110_9I0A7449](/assets/vehicles/1987-land-rover-defender-110-black/1987_Land_Rover_Defender_110_9I0A7449.jpg) -->
![1987_Land_Rover_Defender_110_9I0A7450](/assets/vehicles/1987-land-rover-defender-110-black/1987_Land_Rover_Defender_110_9I0A7450.jpg)
<!-- ![1987_Land_Rover_Defender_110_9I0A7451](/assets/vehicles/1987-land-rover-defender-110-black/1987_Land_Rover_Defender_110_9I0A7451.jpg) -->
![1987_Land_Rover_Defender_110_9I0A7452](/assets/vehicles/1987-land-rover-defender-110-black/1987_Land_Rover_Defender_110_9I0A7452.jpg)
![1987_Land_Rover_Defender_110_9I0A7453](/assets/vehicles/1987-land-rover-defender-110-black/1987_Land_Rover_Defender_110_9I0A7453.jpg)
<!-- ![1987_Land_Rover_Defender_110_9I0A7454](/assets/vehicles/1987-land-rover-defender-110-black/1987_Land_Rover_Defender_110_9I0A7454.jpg) -->
<!-- ![1987_Land_Rover_Defender_110_9I0A7455](/assets/vehicles/1987-land-rover-defender-110-black/1987_Land_Rover_Defender_110_9I0A7455.jpg) -->
![1987_Land_Rover_Defender_110_9I0A7456](/assets/vehicles/1987-land-rover-defender-110-black/1987_Land_Rover_Defender_110_9I0A7456.jpg)

~~~ </div> ~~~


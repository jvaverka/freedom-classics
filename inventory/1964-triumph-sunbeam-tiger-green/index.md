+++
title = "Inventory | 1964 Triumph Sunbeam Tiger (Green)"
vehicle_name = "1964 Triumph Sunbeam Tiger (Green)"
vehicle_photo = "1964_Triumph_Sunbeam_Tiger_9I0A7427"
vehicle_price = "100,000"
vehicle_attr = [
    "264 V8 engine",
    "Removeable Hardtop",
    "Frame off Restoration with documentation",
    "Rare Car",
    "Too many things to list",
]
+++

{{ vehicle_card 1964-triumph-sunbeam-tiger-green }}

## Gallery

~~~ <div class="container"> ~~~

![1964_Triumph_Sunbeam_Tiger_9I0A7423](/assets/vehicles/1964-triumph-sunbeam-tiger-green/1964_Triumph_Sunbeam_Tiger_9I0A7423.jpg)
<!-- ![1964_Triumph_Sunbeam_Tiger_9I0A7424](/assets/vehicles/1964-triumph-sunbeam-tiger-green/1964_Triumph_Sunbeam_Tiger_9I0A7424.jpg) -->
![1964_Triumph_Sunbeam_Tiger_9I0A7425](/assets/vehicles/1964-triumph-sunbeam-tiger-green/1964_Triumph_Sunbeam_Tiger_9I0A7425.jpg)
![1964_Triumph_Sunbeam_Tiger_9I0A7426](/assets/vehicles/1964-triumph-sunbeam-tiger-green/1964_Triumph_Sunbeam_Tiger_9I0A7426.jpg)
<!-- ![1964_Triumph_Sunbeam_Tiger_9I0A7427](/assets/vehicles/1964-triumph-sunbeam-tiger-green/1964_Triumph_Sunbeam_Tiger_9I0A7427.jpg) -->
![1964_Triumph_Sunbeam_Tiger_9I0A7428](/assets/vehicles/1964-triumph-sunbeam-tiger-green/1964_Triumph_Sunbeam_Tiger_9I0A7428.jpg)
![1964_Triumph_Sunbeam_Tiger_9I0A7429](/assets/vehicles/1964-triumph-sunbeam-tiger-green/1964_Triumph_Sunbeam_Tiger_9I0A7429.jpg)
<!-- ![1964_Triumph_Sunbeam_Tiger_9I0A7430](/assets/vehicles/1964-triumph-sunbeam-tiger-green/1964_Triumph_Sunbeam_Tiger_9I0A7430.jpg) -->
![1964_Triumph_Sunbeam_Tiger_9I0A7431](/assets/vehicles/1964-triumph-sunbeam-tiger-green/1964_Triumph_Sunbeam_Tiger_9I0A7431.jpg)
![1964_Triumph_Sunbeam_Tiger_9I0A7432](/assets/vehicles/1964-triumph-sunbeam-tiger-green/1964_Triumph_Sunbeam_Tiger_9I0A7432.jpg)
![1964_Triumph_Sunbeam_Tiger_9I0A7433](/assets/vehicles/1964-triumph-sunbeam-tiger-green/1964_Triumph_Sunbeam_Tiger_9I0A7433.jpg)

~~~ </div> ~~~


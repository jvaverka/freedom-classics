+++
title = "Inventory | 1967 Chevy Chevelle Convertible (Blue)"
vehicle_name = "1967 Chevy Chevelle Convertible (Blue)"
vehicle_photo = "1967_Chevy_Chevelle_Convertible_9I0A7545"
vehicle_price = "75,000"
vehicle_attr = [
    "Dark blue paint",
    "Premium cloth soft top",
    "GM ZZ430 crate engine",
    "Engine is 203 out of 430 engines made.",
]
+++

{{ vehicle_card 1967-chevelle-convert-blue }}

## Gallery

~~~ <div class="container"> ~~~

![1967_Chevy_Chevelle_Convertible_9I0A7542](/assets/vehicles/1967-chevelle-convert-blue/1967_Chevy_Chevelle_Convertible_9I0A7542.jpg)
![1967_Chevy_Chevelle_Convertible_9I0A7544](/assets/vehicles/1967-chevelle-convert-blue/1967_Chevy_Chevelle_Convertible_9I0A7544.jpg)
![1967_Chevy_Chevelle_Convertible_9I0A7545](/assets/vehicles/1967-chevelle-convert-blue/1967_Chevy_Chevelle_Convertible_9I0A7545.jpg)
<!-- ![1967_Chevy_Chevelle_Convertible_9I0A7546](/assets/vehicles/1967-chevelle-convert-blue/1967_Chevy_Chevelle_Convertible_9I0A7546.jpg) -->
![1967_Chevy_Chevelle_Convertible_9I0A7547](/assets/vehicles/1967-chevelle-convert-blue/1967_Chevy_Chevelle_Convertible_9I0A7547.jpg)

~~~ </div> ~~~


+++
title = "Inventory | 1966 Chevy C-10"
vehicle_name = "1966 Chevy C-10 (Gold)"
vehicle_photo = "1966_Chevy_C10"  
vehicle_price = "34,000"
vehicle_attr = [
    "350 rebuilt motor",
    "Air conditioning",
    "New radial tires",
    "Rally wheels",
    "Cloth Seats",
    "Bed cover",
    "Wood Bed",
    "71,600 miles",
    "New paint",
]
+++

{{ vehicle_card 1966-chevy-c-10-gold }}

## Gallery

~~~ <div class="container"> ~~~

![1966_Chevy_C10_a](/assets/vehicles/1966-chevy-c-10-gold/1966_Chevy_C10_a.jpg)
![1966_Chevy_C10_b](/assets/vehicles/1966-chevy-c-10-gold/1966_Chevy_C10_b.jpg)
![1966_Chevy_C10_c](/assets/vehicles/1966-chevy-c-10-gold/1966_Chevy_C10_c.jpg)
![1966_Chevy_C10_d](/assets/vehicles/1966-chevy-c-10-gold/1966_Chevy_C10_d.jpg)
![1966_Chevy_C10_e](/assets/vehicles/1966-chevy-c-10-gold/1966_Chevy_C10_e.jpg)
![1966_Chevy_C10_f](/assets/vehicles/1966-chevy-c-10-gold/1966_Chevy_C10_f.jpg)

~~~ </div> ~~~


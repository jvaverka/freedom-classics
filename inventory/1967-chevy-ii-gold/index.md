+++
title = "Inventory | 1967 Chevy II"
vehicle_name = "1967 Chevy II"
vehicle_photo = "1967_Chevy_II_h"
vehicle_price = "37,000"
vehicle_attr = [
    "350 motor",
    "4 speed manual transmission",
    "373 positive traction rear end",
    "Power steering",
    "Power Brakes",
]
+++

{{ vehicle_card 1967-chevy-ii-gold }}

## Gallery

~~~ <div class="container"> ~~~

<!-- ![1967_Chevy_II_a](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_a.jpg) -->
![1967_Chevy_II_b](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_b.jpg)
<!-- ![1967_Chevy_II_c](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_c.jpg) -->
<!-- ![1967_Chevy_II_d](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_d.jpg) -->
![1967_Chevy_II_e](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_e.jpg)
<!-- ![1967_Chevy_II_f](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_f.jpg) -->
![1967_Chevy_II_g](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_g.jpg)
<!-- ![1967_Chevy_II_h](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_h.jpg) -->
![1967_Chevy_II_i](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_i.jpg)
<!-- ![1967_Chevy_II_j](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_j.jpg) -->
![1967_Chevy_II_k](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_k.jpg)
<!-- ![1967_Chevy_II_l](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_l.jpg) -->
<!-- ![1967_Chevy_II_m](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_m.jpg) -->
![1967_Chevy_II_n](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_n.jpg)
<!-- ![1967_Chevy_II_o](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_o.jpg) -->
<!-- ![1967_Chevy_II_p](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_p.jpg) -->
<!-- ![1967_Chevy_II_q](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_q.jpg) -->
![1967_Chevy_II_r](/assets/vehicles/1967-chevy-ii-gold/1967_Chevy_II_r.jpg)

~~~ </div> ~~~


using Franklin

function hfun_bar(vname)
    val = Meta.parse(vname[1])
    return round(sqrt(val), digits=2)
end

function hfun_m1fill(vname)
    var = vname[1]
    return Franklin.pagevar("index", var)
end

function lx_baz(com, _)
    # keep this first line
    brace_content = Franklin.content(com.braces[1]) # input string
    # do whatever you want here
    return uppercase(brace_content)
end

@delay function hfun_vehicle_card(vehicle)
    vehicle_name = vehicle[1]
    vehicle_page = "inventory/$(vehicle_name)/index"
    @info vehicle_page
    name = Franklin.pagevar(vehicle_page, "vehicle_name")
    photo = Franklin.pagevar(vehicle_page, "vehicle_photo")
    price = Franklin.pagevar(vehicle_page, "vehicle_price")
    attr = Franklin.pagevar(vehicle_page, "vehicle_attr")
    io = IOBuffer()
    write(io, "<div class=\"container\">")
    write(io, "<h1>$name</h1>")
    write(io, "<img src=\"/assets/vehicles/$(vehicle_name)/$(photo).jpg\" alt=\"$(name)\"></img>")
    write(io, "<h3>Details</h3>")
    write(io, "<ul>")
    for a in attr
        write(io, "<li>$a</li>")
    end
    write(io, "<hr width=\"50%\">")
    write(io, "<li><strong>\$ $price</strong></li>")
    write(io, "</ul>")
    write(io, "</div>") # container
    return String(take!(io))
end
